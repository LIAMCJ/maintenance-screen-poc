# Maintenance-Screen-Poc

After multiple seraches i was able to find the most efficient implementation of a maintenance page for angular by 
using conditional routing which i came across in at the following stackoverflow question 
at the second answer with 40 upvotes 
https://stackoverflow.com/questions/34660263/angular2-conditional-routing
they attached a link to a stackblitz example which made it easier to understand

I see it as the most efficient since it requires very little configuration to any angular app and is easy to 
understand with the proper explanation

The app is switched to maintenance mode by changign the boolean variable to true 