import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { MaintenanceViewComponent } from './views/maintenance-view/maintenance-view.component';
import { MainComponent } from './features/main/main.component';
import { NavBarComponent } from './features/nav-bar/nav-bar.component';
import { HeaderComponent } from './features/header/header.component';

import {AuthGuardMaintenance} from './services/auth-guard.service';
import { SubjectsComponent } from './features/subjects/subjects.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MaintenanceViewComponent,
    MainComponent,
    NavBarComponent,
    HeaderComponent,
    SubjectsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [AuthGuardMaintenance],
  bootstrap: [AppComponent]
})
export class AppModule { }
