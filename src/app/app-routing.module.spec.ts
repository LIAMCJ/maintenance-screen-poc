import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";

import { AuthService } from './services/auth.service';
import { AuthGuardMaintenance } from './services/auth-guard.service'

class MockRouter {
  navigate(path) {}
}

describe('Router: App', () => {
  let authGuard: AuthGuardMaintenance;
  let authService;
  let router;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [AuthGuardMaintenance]
    });
  });

  it('should return true and redirect to root page when inMaintenance is false',() => {
    authService = {inMaintenance: () => false }
    router = new MockRouter();
    authGuard = new AuthGuardMaintenance(authService, router);
    spyOn(router, 'navigate');

    expect(authGuard.canActivate()).toEqual(true);
  });

  it('should return false and redirect to maintenance page when inMaintenance is true',() => {
    authService = {inMaintenance: () => true }
    router = new MockRouter();
    authGuard = new AuthGuardMaintenance(authService, router);
    spyOn(router, 'navigate');

    expect(authGuard.canActivate()).toEqual(false);
    expect(router.navigate).toHaveBeenCalledWith(['/maintenance']);

  });

});
