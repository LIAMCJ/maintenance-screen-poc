export class Subject{
	id:string;
	image:string;
	title:string;
	overview:string;
	url:string;
}