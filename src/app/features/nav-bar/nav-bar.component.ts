import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  // used to determine wheter user is logged in or not
  isLoggedIn:Boolean = false;
  // the message that is displayed in log in button
  logInMessage:string = "Log In"

  constructor() { }

  ngOnInit(): void {
  }

  logIn(): void{
	this.isLoggedIn = !this.isLoggedIn;

	if(this.isLoggedIn){
		this.logInMessage = "Log Out"
	}
	else{
		this.logInMessage = "Log In"
	}
  }
}

