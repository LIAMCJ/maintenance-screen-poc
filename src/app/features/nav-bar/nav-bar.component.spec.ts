import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { NavBarComponent } from './nav-bar.component';

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle the log in button', fakeAsync(() => {
      expect(component.isLoggedIn).toBeFalse();
      component.logIn();
      tick(500);
      fixture.detectChanges();
      expect(component.isLoggedIn).toBeTrue();
    }))
});
