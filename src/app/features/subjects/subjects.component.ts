import { Component, OnInit } from '@angular/core';
import {Subject} from 'src/app/types/Subject';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {
  subjects:Subject[]
  constructor() { }

  ngOnInit(): void {

    this.subjects = [
      {id:"html", image:"assets/images/htmlWallpaper.jpg", title:"HTML", overview:"Hypertext Markup Language is the standard markup language for documents designed to be displayed in a web browse", url:"https://www.w3schools.com/html/"},
      {id:"javaS", image:"assets/images/javascriptWallpaper.jpg", title:"JavaScript", overview:"JavaScript is a client scripting language which is used for creating web pages", url:"https://www.w3schools.com/js/"},
      {id:"css", image:"assets/images/cssWallpaper.jpg", title:"CSS", overview:"Cascading Style Sheets is a style sheet language used for describing the presentation of HTML", url:"https://www.w3schools.com/css/"}
    ]

  }

}
