import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectsComponent } from './subjects.component';

describe('SubjectsComponent', () => {
  let component: SubjectsComponent;
  let fixture: ComponentFixture<SubjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain proper properties', () => {
    const compiledComponent = fixture.nativeElement
    component.subjects.map((subjectObject) => {
      const subjectPosition = "#" + subjectObject.id + " h4";
      expect(compiledComponent.querySelector(subjectPosition).textContent).toContain(subjectObject.title)
    })
  })
});
