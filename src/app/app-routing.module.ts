import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { MaintenanceViewComponent } from './views/maintenance-view/maintenance-view.component';
import { MainComponent } from './features/main/main.component';

import {AuthGuardMaintenance} from './services/auth-guard.service'

export const routes: Routes = [
  { path: '', component: MainComponent, canActivate: [AuthGuardMaintenance] },
  { path: 'maintenance', component: MaintenanceViewComponent },
  { path: '**', component: PageNotFoundComponent },
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
